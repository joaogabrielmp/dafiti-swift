
import Foundation
import UIKit
protocol BestBuyProductsCollectionViewDataSourceDelegate : class {
    func didSelectBestBuyProduct(product:BestBuyProductProtocol)
    func didScroll(scrollView:UIScrollView)

}