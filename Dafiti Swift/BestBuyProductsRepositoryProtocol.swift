
import Foundation
protocol BestBuyProductsRepositoryProtocol {
    static func getBestBuyProducts ( completion: (result: [BestBuyProductProtocol]) -> Void, error: (error: String) -> Void)
}