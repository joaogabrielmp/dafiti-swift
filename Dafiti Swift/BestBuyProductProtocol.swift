

import Foundation
protocol BestBuyProductProtocol: class {
    func getBrand() -> String
    func getName() -> String
    func getProductDescription() -> String
    func getSalePrice() -> Float
    func getThumbnailImage() -> String
    func getImage() -> String
    func getUrlDetail() -> String
    
    func brand(brand:String)
    func name(name:String)
    func productDescription(productDescription:String)
    func salePrice(salePrice:Float)
    func thumbnailImage(thumbnailImage:String)
    func image(image:String)
    func urlDetail(urlDetail:String)
    
}