

import UIKit

class DetailBestBuyProductsViewController: UIViewController {
    
    //MARK: ui properties
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var brandLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var imageView: UIImageView!
    
    //MARK: properties
    var bestBuyProduct: BestBuyProductProtocol?
    
    func setProduct(bestBuyProduct: BestBuyProductProtocol) {
        self.bestBuyProduct = bestBuyProduct
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Detalhe"
        loadImageFromUrl((bestBuyProduct?.getImage())!, view: self.imageView)
        nameLabel.text = bestBuyProduct?.getName()
        brandLabel.text = bestBuyProduct?.getBrand()
        let salePrice = bestBuyProduct?.getSalePrice()
        let salePriceString = String(format: "U$%.2f",salePrice!)
        priceLabel.text = salePriceString
        descriptionLabel.text = bestBuyProduct?.getProductDescription()
        // Do any additional setup after loading the view.
    }
    
    func loadImageFromUrl(url: String, view: UIImageView){
        
        let url = NSURL(string: url)!
        
        let task = NSURLSession.sharedSession().dataTaskWithURL(url) { (responseData, responseUrl, error) -> Void in
            
            if let data = responseData{
                
                dispatch_async(dispatch_get_main_queue(), { () -> Void in
                    view.image = UIImage(data: data)
                })
            }
        }
        task.resume()
    }


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
