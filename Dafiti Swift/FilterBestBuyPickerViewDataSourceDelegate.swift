
import Foundation
import UIKit
protocol FilterBestBuyPickerViewDataSourceDelegate : class {
    func didSelectBestBuyProductFilter(filter:String)
}