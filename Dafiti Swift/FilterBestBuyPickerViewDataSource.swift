

import Foundation
import UIKit

class FilterBestBuyPickerViewDataSource: NSObject,UIPickerViewDataSource,UIPickerViewDelegate {
    //MARK: properties
    weak var delegate : FilterBestBuyPickerViewDataSourceDelegate?
    private var filters: [String]?
    var pickerView : UIPickerView?

    //MARK: constructors
    internal init(pickerView:UIPickerView) {
        super.init()
        self.pickerView = pickerView
        self.pickerView?.delegate = self
        self.pickerView?.dataSource = self
        
    }
    
    //MARK: getters and setters
    func setFilters(filters:[String]) {
        self.filters = filters
        self.pickerView?.reloadAllComponents()
    }
    
    func getFilters() -> [String] {
        if (self.filters != nil) {
            return self.filters!
        }
        self.filters = [String]()
        return self.filters!
    }

    //MARK: picker view data source
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if (filters != nil) {
            return filters!.count;
        }
        return 0
        
    }
    
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
       return filters![row]
    }
    
    //MARK: picker view delegate
    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int)
    {
        delegate?.didSelectBestBuyProductFilter(filters![row])
    }

}

