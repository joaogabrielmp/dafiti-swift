
import Foundation
import UIKit
class BestBuyProductsCollectionViewDataSource : NSObject,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    //MARK: properties
    weak var delegate : BestBuyProductsCollectionViewDataSourceDelegate?
    private var bestBuyProducts: [BestBuyProductProtocol]?
    var collectionView : UICollectionView?
    
    //MARK: constructors
    internal init(collectionView:UICollectionView) {
        super.init()
        self.collectionView = collectionView
        self.collectionView?.delegate = self
        self.collectionView?.dataSource = self
        self.collectionView?.registerNib(UINib.init(nibName: "BestBuyProductCollectionCell", bundle: nil), forCellWithReuseIdentifier: "cell")
        let flow:UICollectionViewFlowLayout = UICollectionViewFlowLayout.init()
        flow.scrollDirection = UICollectionViewScrollDirection.Vertical
        flow.minimumInteritemSpacing = 4
        flow.minimumLineSpacing = 8
        flow.sectionHeadersPinToVisibleBounds = false
        self.collectionView?.collectionViewLayout = flow
        
    }
    
    //MARK: getters and setters
    func setProducts(products:[BestBuyProductProtocol]) {
        self.bestBuyProducts = products
        self.collectionView?.reloadData()
    }
    
    func getProducts() -> [BestBuyProductProtocol] {
        if (self.bestBuyProducts != nil) {
            return self.bestBuyProducts!
        }
        self.bestBuyProducts = [BestBuyProductProtocol]()
        return self.bestBuyProducts!
    }
    
    //MARK: collection view data source
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if (self.bestBuyProducts != nil) {
            return (self.bestBuyProducts?.count)!
        }
            return 0
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
       let cell : BestBuyProductCollectionCell = (collectionView.dequeueReusableCellWithReuseIdentifier("cell", forIndexPath: indexPath) as? BestBuyProductCollectionCell)!
        let bestBuyProduct : BestBuyProductProtocol = self.bestBuyProducts![indexPath.row]
        cell.setImageURL(bestBuyProduct.getImage())
        cell.setNameProduct(bestBuyProduct.getName())
        cell.setBrandProduct(bestBuyProduct.getBrand())
        
        let salePrice = bestBuyProduct.getSalePrice()
        let salePriceString = String(format: "U$%.2f",salePrice)
        cell.setPriceProduct(salePriceString)
        return cell
    }
    
    //MARK : collection view delegate
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        let bestBuyProduct : BestBuyProductProtocol = self.bestBuyProducts![indexPath.row]
        self.delegate?.didSelectBestBuyProduct(bestBuyProduct)
    }
    
    //MARK : collection view flow layout delegate
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAtIndex section: Int) -> UIEdgeInsets {
        return UIEdgeInsetsMake(8, 8, 8, 8)
    }
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        let cellsAcross : CGFloat = 2;
        let spaceBetweenCells : CGFloat  = 24;
        let width : CGFloat  = (collectionView.bounds.width - (cellsAcross - 1) * spaceBetweenCells) / cellsAcross;
            return CGSizeMake(width, 280);
    }
    
    //MARK : scroll view delegate
    func scrollViewDidScroll(scrollView: UIScrollView) {
        self.delegate?.didScroll(scrollView)
    }
}
