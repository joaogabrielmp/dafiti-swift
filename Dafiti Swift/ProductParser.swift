
import UIKit

class ProductParser: NSObject {
    static func bestBuyProductsParsed(response : [String:AnyObject]) ->  [BestBuyProductProtocol] {
        var products:[BestBuyProductProtocol] = [BestBuyProductProtocol]();
        if let bestBuyProducts = response["products"] as? [[String: AnyObject]] {
            for product in bestBuyProducts {
                let bestBuyProduct:BestBuyProduct = BestBuyProduct()
                if let name = product["name"] as? String{
                bestBuyProduct.name(name)
                }
                print(product.keys)
                if let salePrice = product["salePrice"] as? Float {
                    bestBuyProduct.salePrice(salePrice)
                } 
                if let thumbnailImage = product["thumbnailImage"] as? String {
                    bestBuyProduct.thumbnailImage(thumbnailImage)
                } 
                if let image = product["image"] as? String {
                    bestBuyProduct.image(image)
                } else {
                    bestBuyProduct.image("http://placehold.it/300?text=Nenhuma+Imagem+Encontrada")
                }
                if let url = product["url"] as? String {
                    bestBuyProduct.urlDetail(url)
                } 

                if let longDescription = product["longDescription"] as? String {
                    bestBuyProduct.productDescription(longDescription)
                } 

                if let details = product["details"] as? [[String: AnyObject]] {
                    for detail in details {
                        if let detailName = detail["name"] as? String {
                            if detailName == "Brand" {
                                if let brand = detail["value"] as? String {
                                    bestBuyProduct.brand(brand)
                                } 
                            }
                        } 
                    }
                }
                products.append(bestBuyProduct)
            }
        }
        return products;
    }
}