

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var navController : UINavigationController?
    var bestBuyProductsViewController : BestBuyProductsViewController?
    
    
    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        window = UIWindow.init(frame: UIScreen.mainScreen().bounds)
        window?.backgroundColor = UIColor.whiteColor()
        window?.makeKeyAndVisible()
        bestBuyProductsViewController = BestBuyProductsViewController()
        navController = UINavigationController.init(rootViewController: bestBuyProductsViewController!)
        window?.rootViewController = navController
        applicationApplyAppearanceChanges()
        return true
    }
    func applicationApplyAppearanceChanges() {
        UINavigationBar.appearance().tintColor = UIColor.init(red: 0.3147, green:0.3147, blue:0.3147,
                                                              alpha:1.0)
        UINavigationBar.appearance().barTintColor = UIColor.init(red: 0.9609, green:0.9756, blue:0.9753,
                                                                 alpha:1.0)
        UINavigationBar.appearance().titleTextAttributes = [NSForegroundColorAttributeName : UIColor.init(red: 0.3147, green: 0.3147, blue: 0.3147, alpha: 1.0)]

        UISearchBar.appearance().tintColor = UIColor.ColorLight()
        UIToolbar.appearance().tintColor = UIColor.ColorDark()
        UINavigationBar.appearance().translucent = false
    
    }
    func applicationWillResignActive(application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }


}

