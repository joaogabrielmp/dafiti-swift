

import UIKit

class BestBuyProductCollectionCell: UICollectionViewCell {
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var brandLabel: UILabel!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    //MARK: setters
    func setImageURL(imageURL:String)  {
       loadImageFromUrl(imageURL, view: self.imageView)
    }
    
    func loadImageFromUrl(url: String, view: UIImageView){
        
        let url = NSURL(string: url)!
        
        let task = NSURLSession.sharedSession().dataTaskWithURL(url) { (responseData, responseUrl, error) -> Void in

            if let data = responseData{

                dispatch_async(dispatch_get_main_queue(), { () -> Void in
                    view.image = UIImage(data: data)
                })
            }
        }
        task.resume()
    }
    
    func setBrandProduct(brand:String)  {
        self.brandLabel.text = brand
    }
    
    func setNameProduct(name:String)  {
        self.nameLabel.text = name
    }
    
    func setPriceProduct(price:String)  {
        self.priceLabel.text = price
    }
}
