
import UIKit

class BestBuyProductsViewController: UIViewController,BestBuyProductsCollectionViewDataSourceDelegate,FilterBestBuyPickerViewDataSourceDelegate,UISearchBarDelegate {
    //MARK: ui properties
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var filterPickerView: UIPickerView!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var alertLabel: UILabel!
    @IBOutlet weak var searchButton: UIButton!
    @IBOutlet weak var filterButton: UIButton!
    
    @IBOutlet weak var filterTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var searchTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var searchBarHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var listCollectionViewBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var pickerViewBottomConstraint: NSLayoutConstraint!
    
    //MARK: properties
    var bestBuyProducts:[BestBuyProductProtocol] = []
    var bestBuyProductsCollectionViewDataSource:BestBuyProductsCollectionViewDataSource?
    var filterBestBuyPickerViewDataSource:FilterBestBuyPickerViewDataSource?
    var lastContentOffset : CGFloat?
    
    //MARK: life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Lista de Produtos"
        setCollectionViewDataSource()
        setFilterPickerViewDataSource()
        getProducts()
        setBorders()
        self.searchBarHeightConstraint.constant = 0
        self.searchBar.delegate = self
        setSearchBarToolBar()
        hideButtonsWithTime(0.0)
        hideFilter(0.0)
        self.view.layoutIfNeeded()
    }
    
    func setFilterPickerViewDataSource() {
        self.filterBestBuyPickerViewDataSource = FilterBestBuyPickerViewDataSource(pickerView:self.filterPickerView)
        self.filterBestBuyPickerViewDataSource?.delegate = self
        self.filterBestBuyPickerViewDataSource?.setFilters(["Valor Crescente","Valor Decrescente"])
    }
    
    func setCollectionViewDataSource() {
    self.bestBuyProductsCollectionViewDataSource = BestBuyProductsCollectionViewDataSource(collectionView: self.collectionView)
    self.bestBuyProductsCollectionViewDataSource?.delegate = self
    }
    
    func getProducts() {
        BestBuyProductsRepository.getBestBuyProducts({ (result) in
            self.bestBuyProducts = result
            self.bestBuyProductsCollectionViewDataSource?.setProducts(result)
            self.showButtonsWithTime(0.4)
            }) { (error) in
                UIAlertView.init(title: "Alerta", message: error, delegate: self, cancelButtonTitle: "OK").show()
                self.searchButton.hidden = true
                self.searchButton.hidden = true
        }
    
    }

    //MARK : set layout
    func setSearchBarToolBar() {
        let doneButton : UIBarButtonItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.Done, target: self, action: #selector(BestBuyProductsViewController.searchBarDoneAction))
        let toolBar : UIToolbar = UIToolbar()
        toolBar.sizeToFit()
        toolBar.setItems([doneButton], animated: true)
        self.searchBar.inputAccessoryView = toolBar as UIView
        
    }

    override func viewDidLayoutSubviews() {
         self.collectionView.alwaysBounceVertical = true
    }
    
    func setBorders() {
        self.collectionView.layer.cornerRadius = 5.0

        let searchShapePath : UIBezierPath = UIBezierPath.init(roundedRect: CGRectMake(self.searchButton.bounds.origin.x, self.searchButton.bounds.origin.y, self.searchButton.bounds.size.width, self.searchButton.bounds.size.height), byRoundingCorners:UIRectCorner.AllCorners, cornerRadii: CGSizeMake(35.0, 35.0))
        
        let searchShapeLayer : CAShapeLayer = CAShapeLayer()
        searchShapeLayer.frame = self.searchButton.bounds
        searchShapeLayer.path = searchShapePath.CGPath
        searchShapeLayer.fillColor = UIColor.Color().CGColor
        self.searchButton.layer.insertSublayer(searchShapeLayer, atIndex: 0)
        
        let filterShapePath : UIBezierPath = UIBezierPath.init(roundedRect: CGRectMake(self.filterButton.bounds.origin.x, self.filterButton.bounds.origin.y, self.filterButton.bounds.size.width, self.filterButton.bounds.size.height), byRoundingCorners:UIRectCorner.AllCorners, cornerRadii: CGSizeMake(35.0, 35.0))
        
        let filterShapeLayer : CAShapeLayer = CAShapeLayer()
        filterShapeLayer.frame = self.filterButton.bounds
        filterShapeLayer.path = filterShapePath.CGPath
        filterShapeLayer.fillColor = UIColor.Color().CGColor
        self.filterButton.layer.insertSublayer(filterShapeLayer, atIndex: 0)
    
    }

    func showButtonsWithTime(timer:Double) {
        self.filterTopConstraint.constant = 55
        self.searchTopConstraint.constant = 55
        UIView.animateWithDuration(timer) {
            self.view.layoutIfNeeded()
        }
    }
    
    func hideButtonsWithTime(timer:Double) {
        self.filterTopConstraint.constant = 200
        self.searchTopConstraint.constant = 200
        UIView.animateWithDuration(timer) {
            self.view.layoutIfNeeded()
        }
    }
    
    func showSearchBar() {
        self.searchBarHeightConstraint.constant = 44
        UIView.animateWithDuration(0.4) {
            self.view.layoutIfNeeded()
        }
    }
    
    func hideSearchBar() {
        self.searchBarHeightConstraint.constant = 0
        UIView.animateWithDuration(0.4) {
            self.view.layoutIfNeeded()
        }
    }
    
    func showKeyBoard() {
        if (UIDeviceOrientationIsLandscape(UIDevice.currentDevice().orientation))
        {
        self.listCollectionViewBottomConstraint.constant = 216
        } else {
        self.listCollectionViewBottomConstraint.constant = 268
        }
        self.view.layoutIfNeeded()
    }
    
    func hideKeyBoard() {
        self.listCollectionViewBottomConstraint.constant = 8
        self.view.layoutIfNeeded()
    }

    func showFilter(timer:Double) {
         self.listCollectionViewBottomConstraint.constant = 172
        self.pickerViewBottomConstraint.constant = 0
        UIView.animateWithDuration(timer) {
            self.view.layoutIfNeeded()
        }
    }
    
    func hideFilter(timer:Double) {
         self.listCollectionViewBottomConstraint.constant = 8
        self.pickerViewBottomConstraint.constant = -164
        UIView.animateWithDuration(timer) {
            self.view.layoutIfNeeded()
        }
    }

    //MARK: Actions
    @IBAction func searchAction(sender: UIButton) {
        showSearchBar()
        self.searchBar.becomeFirstResponder()
    }
    
    @IBAction func filterAction(sender: UIButton) {
        showFilter(0.4)
    }
    
    func searchBarDoneAction()  {
        hideKeyBoard()
        self.collectionView.reloadData()
        self.searchBar.resignFirstResponder()
    }

    @IBAction func filterPickerViewAction(sender: UIBarButtonItem) {
        hideFilter(0.4)
    }
    
    //MARK: search bar delegate
    func searchBar(searchBar: UISearchBar, textDidChange searchText: String) {
       var bestBuyProductsNameFilteredArray : [BestBuyProductProtocol] = self.bestBuyProducts
        if searchText != "" {
            let predicate : NSPredicate = NSPredicate(format: "name CONTAINS[cd] %@", searchText)
            bestBuyProductsNameFilteredArray = self.bestBuyProducts.filter{
                return predicate.evaluateWithObject($0)
            }
            if bestBuyProductsNameFilteredArray.count == 0 {
                self.alertLabel.text = "Nenhum Produto encontrado.."
                self.collectionView.hidden = true
            } else {
                self.collectionView.hidden = false
            }
            self.bestBuyProductsCollectionViewDataSource?.setProducts(bestBuyProductsNameFilteredArray)
        } else {
            self.collectionView.hidden = false
            self.bestBuyProductsCollectionViewDataSource?.setProducts(self.bestBuyProducts)
        }
    }
    
    func searchBarTextDidBeginEditing(searchBar: UISearchBar) {
       showKeyBoard()
    }
    
    //MARK: BestBuyProductsCollectionViewDataSourceDelegate
    func didSelectBestBuyProduct(product: BestBuyProductProtocol) {
        hideFilter(0.4)
        hideKeyBoard()
        let detailBestBuyProductsViewController : DetailBestBuyProductsViewController = DetailBestBuyProductsViewController()
        detailBestBuyProductsViewController.setProduct(product)
        self.navigationController?.pushViewController(detailBestBuyProductsViewController, animated: true)
    }
    
    func didScroll(scrollView: UIScrollView) {
        if (scrollView.contentOffset.y < -55) {
            showSearchBar()
        }
        if (self.lastContentOffset > scrollView.contentOffset.y )
        {
            showButtonsWithTime(0.4)
        }
        else if (self.lastContentOffset < scrollView.contentOffset.y && scrollView.contentOffset.y>0)
        {
            hideButtonsWithTime(0.4)
            hideSearchBar()
        }

        self.lastContentOffset = scrollView.contentOffset.y
    }
    
    //MARK: BestBuyProductsCollectionViewDataSourceDelegate
    func didSelectBestBuyProductFilter(filter: String) {
        if filter == "Valor Crescente" {
           sortInOrderASC(true)
        } else {
           sortInOrderASC(false)
        }
    }
    
    func sortInOrderASC(asc: Bool) {
        let bestBuyProductsNameFilteredArray : [BestBuyProductProtocol] = self.bestBuyProducts
        if asc == true {
            let sortedBestBuyProductsNameFilteredArray=bestBuyProductsNameFilteredArray.sort { (bestBuyProduct1, bestBuyProduct2) -> Bool in
                return (bestBuyProduct1.getSalePrice()) < (bestBuyProduct2.getSalePrice())
            }
            self.bestBuyProductsCollectionViewDataSource?.setProducts(sortedBestBuyProductsNameFilteredArray)
        } else {
            let sortedBestBuyProductsNameFilteredArray=bestBuyProductsNameFilteredArray.sort { (bestBuyProduct1, bestBuyProduct2) -> Bool in
                return (bestBuyProduct1.getSalePrice()) > (bestBuyProduct2.getSalePrice())
            }
            self.bestBuyProductsCollectionViewDataSource?.setProducts(sortedBestBuyProductsNameFilteredArray)
        }
        
        
    }
  
    //MARK: dealloc
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}