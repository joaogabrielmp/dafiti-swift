

import UIKit

extension BestBuyProduct: BestBuyProductProtocol {
    //MARK: getters
    func getBrand() -> String {
        return brand
    }
    func getName() -> String {
        return name
    }
    func getSalePrice() -> Float {
        return salePrice
    }
    func getThumbnailImage() -> String {
        return thumbnailImage
    }
    func getImage() -> String {
        return image
    }
    func getUrlDetail() -> String {
        return urlDetail
    }
    func getProductDescription() -> String {
        return productDescription
    }
    
    func name(name:String) {
        self.name = name;
    }
    func brand(brand:String) {
        self.brand = brand;
    }
    func salePrice(salePrice:Float) {
        self.salePrice = salePrice;
    }
    func thumbnailImage(thumbnailImage:String) {
        self.thumbnailImage = thumbnailImage;
    }
    func image(image:String) {
        self.image = image;
    }
    func urlDetail(urlDetail:String) {
        self.urlDetail = urlDetail;
    }
    func productDescription(productDescription:String) {
        self.productDescription = productDescription;
    }
}

class BestBuyProduct: NSObject {
    
    //MARK : properties
    private var brand: String!
    private var name: String!
    private var salePrice: Float!
    private var thumbnailImage: String!
    private var image: String!
    private var urlDetail: String!
    private var productDescription: String!
    
}
