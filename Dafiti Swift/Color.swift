
import UIKit

extension UIColor {
    class func colorWithRGB(red: CGFloat, _ green: CGFloat, _ blue: CGFloat, _ alpha: CGFloat) -> UIColor {

        return UIColor.init(red: red, green: green, blue: blue, alpha: alpha)
    }
    class func Color() -> UIColor {
        return UIColor.colorWithRGB(205/255.0, 220/255.0, 57/255.0, 1.0)
    }
    class func ColorDark() -> UIColor {
        return UIColor.colorWithRGB(192/255.0, 202/255.0, 51/255.0, 1.0)
    }
    class func ColorLight() -> UIColor {
        return UIColor.colorWithRGB(212/255.0, 225/255.0, 87/255.0, 1.0)
    }
}

//+(UIColor *)colorWithRGBRed:(float)red green:(float)green blue:(float)blue andAlpha:(float)alpha {
//    return [UIColor colorWithRed:red/255.0f green:green/255.0f blue:blue/255.0f alpha:alpha];
//}
//+(UIColor *)Color {
//    return [UIColor colorWithRGBRed:205 green:220 blue:57 andAlpha:1.000];
//}
//+(UIColor *)ColorDark {
//    return [UIColor colorWithRGBRed:192 green:202 blue:51 andAlpha:1.000];
//}
//+(UIColor *)ColorLight {
//    return [UIColor colorWithRGBRed:212 green:225 blue:87 andAlpha:1.000];
//}
