
import UIKit
import Alamofire

class BestBuyProductsRepository: NSObject,BestBuyProductsRepositoryProtocol {
    static func getBestBuyProducts ( completion: (result: [BestBuyProductProtocol]) -> Void, error: (error: String) -> Void){
        Alamofire.request(.GET, "https://api.bestbuy.com/v1/products(categoryPath.name=All%20Flat-Panel%20TVs)?show=all&format=json&apiKey=hz7ubm2z7yepkj9evg88x9fk")
            .responseJSON { response in switch response.result {
            case .Success( _):
                if let JSON = response.result.value {
                    let bestBuyProducts = JSON as? [String: AnyObject]
                    completion(result: ProductParser.bestBuyProductsParsed(bestBuyProducts!))
                } else {
                    error(error: "Algum erro ocorreu!")
                }
            case .Failure( _):
                error(error: "Algum erro ocorreu!")
        }
            }
    }
}
